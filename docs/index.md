# Python 데이터 시각화: Matplotlib, Seaborn <sup>[1](#footnote_1)</sup>

> <font size="3">Python에서 Matplotlib과 Seaborn 라이브러리를 사용하여 데이터 시각화를 만드는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./data-visualization.md#intro)
1. [데이터 시각화란?](./data-visualization.md#sec_02)
1. [데이터 시각화를 위해 Python을 사용하는 이유](./data-visualization.md#sec_03)
1. [Matplotlib과 Seaborn 설치와 Import](./data-visualization.md#sec_04)
1. [Matplotlib을 사용한 기본 플롯팅](./data-visualization.md#sec_05)
1. [Matplotlib에서 플롯 커스터마이징](./data-visualization.md#sec_06)
1. [Seaborn을 사용한 통계 플롯](./data-visualization.md#sec_07)
1. [Seaborn을 사용한 고급 플롯](./data-visualization.md#sec_08)
1. [Matplotlib과 Seaborn 결합](./data-visualization.md#sec_09)
1. [요약](./data-visualization.md#summary)

<a name="footnote_1">1</a>: [Python Tutorial 45 — Python Data Visualization: Matplotlib, Seaborn](https://python.plainenglish.io/python-tutorial-45-python-data-visualization-matplotlib-seaborn-d79782ac7906?sk=a7f66697f9cae924d7be09dd152ef5fd)를 편역하였습니다.
