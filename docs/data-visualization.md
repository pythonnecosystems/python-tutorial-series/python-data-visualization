# Python 데이터 시각화: Matplotlib, Seaborn

## <a name="intro"></a> 개요
Matplotlib와 Seaborn을 이용한 Python 데이터 시각화에 관한 이 포스팅에서는 Python에서 Matplotlib와 Seaborn 라이브러리를 이용하여 데이터 시각화를 만드는 방법을 설명한다. 또한 플롯을 커스터마이징하고 통계 정보를 추가하며 다양한 유형의 플롯을 결합하는 방법도 보인다.

데이터 시각화는 데이터와 정보의 그래픽 표현을 만드는 과정이다. 데이터 시각화는 데이터를 명확하고 효과적인 방법으로 이해하고 탐색하며 전달하는 데 도움을 줄 수 있다. 데이터 시각화는 데이터에서 패턴, 추세, 이상치 및 관계를 발견하는 데 도움을 줄 수 있다.

Python은 데이터 분석과 데이터 과학 분야에서 주로 사용되는 프로그래밍 언어이다. Python에는 데이터 조작, 계산 및 시각화 작업을 수행하는 데 도움을 줄 수 있는 많은 라이브러리와 도구가 있다. Python에서 데이터 시각화 작업에 가장 널리 사용되는 두 가지 라이브러리는 Matplotlib와 Seaborn이다.

Matplotlib는 다양한 유형의 플롯과 차트를 생성하기 위한 포괄적인 도구 집합을 제공하는 저수준 라이브러리이다. Matplotlib는 커스터마이징이 가능하고 유연하여 플롯의 모든 측면을 제어할 수 있다. Matplotlib는 플롯을 하기 위해 Matplotlib를 사용하는 Seaborn, Pandas, scikit-learn과 같은 다른 많은 고수준 라이브러리의 기반이기도 하다.

Seaborn은 높은 수준의 라이브러리로, 통계 플롯과 차트를 작성하는 데 더 간단하고 우아한 인터페이스를 제공한다. Seaborn은 Matplotlib 위에 구축되어 있으며 Python에서 데이터 분석을 위한 주요 데이터 구조와 라이브러리인 Pandas와 Numpy와 잘 결합되어 있다. Seaborn은 또한 플롯을 더 매력적이고 전문적으로 만들 수 있는 다양한 테마와 스타일을 제공한다.

이 포스팅에서는 Matplotlib와 Seaborn을 모두 사용하여 Python에서 데이터 시각화를 만드는 방법을 설명할 것이다. 또한 두 라이브러리의 장점을 결합하여 더 고급스럽고 정교한 플롯을 만드는 방법도 다룰 것이다. 이를 학습하면 다음과 같은 작업을 수행할 수 있다.

- Matplotlib와 Seaborn 설치와 임포트
- 선 그림, 산점도, 막대 그림 및 히스토그램과 같은 Matplotlib을 사용하여 기본 플롯 생성
- 제목, 레이블, 범례, 축, 격자 및 주석 추가와 같은 Matplotlib을 사용하여 플롯 커스터마이징
- 상자 그림, 바이올린 그림, 무리 그림, 히트맵로 통계 플롯을 작성
- 쌍도, 합동도, 회귀분석도, 요인도 등 고급 플롯 생성
- Matplotlib와 Seaborn을 결합하여 부분구도, 패싯 그리드 및 관계 플롯 같이 더 복잡하고 상호작용적인 그림 생성

이 포스팅의 내용을 시험하려면 컴퓨터에 Python을 설치해야 한다. Matplotlib와 Seaborn도 설치해야 하며, 아니면 이러한 라이브러리가 미리 설치된 구Google Colab 이나 주피터 노트북 같은 온라인 플랫폼을 사용할 수도 있다. 변수, 데이터 유형, 함수, 루프, 조건문을 사용하는 방법, Pandas와 Numpy를 사용하는 방법 등 Python과 데이터 분석에 대한 기본 지식도 갖춰야 한다.

Matplotlib와 Seaborn으로 데이터 시각화 작업을 시작할 준비가 되었나요? 시작하겠다!

## <a name="sec_02"></a> 데이터 시각화란?
데이터 시각화는 데이터와 정보의 그래픽 표현을 만드는 과정이다. 데이터 시각화는 데이터를 명확하고 효과적인 방법으로 이해하고 탐색하며 전달하는 데 도움을 줄 수 있다. 나아가서 데이터에서 패턴, 추세, 이상치 및 관계를 발견하는 데 도움을 줄 수 있다.

데이터 시각화는 데이터의 종류, 크기, 복잡성, 시각화의 목적과 청중에 따라 여러 가지로 이루어질 수 있다. 데이터 시각화의 가장 일반적인 유형은 다음과 같다.

- **선 그래프(line plot)**: 선 그래프는 변수가 시간에 따라 또는 연속형 척도를 따라 어떻게 변화하는지 보인다. 선 그래프는 데이터의 추세, 변동 및 패턴을 보여주는 데 유용하다.
- **산점도(scatter plot)**: 산점도는 두 변수 간의 관계를 2차원 평면에 점으로 표시하여 보여준다. 산점도는 데이터의 상관관계, 군집 및 이상치를 보여주는 데 유용하다.
- **막대 그래프(bar plot)**: 막대 그림은 범주형 변수의 값을 다른 높이의 막대로 나타내어 분포를 보여준다. 막대 그림은 데이터의 비교, 비율 및 빈도를 보여주는 데 유용하다.
- **히스토그램(histogram)**: 히스토그램은 수치 변수의 값을 빈으로 그룹화하여 여러 높이의 막대로 표시함으로써 분포를 보여준다. 히스토그램은 자료의 모양, 퍼짐, 왜도를 나타내는데 유용하다.
- **상자 그래프(box plot)**: 상자 그래프는 최소, 최대값, 중앙값 및 사분위수를 수염이 있는 상자로 표시하여 수치 변수의 요약 통계량을 보여준다. 상자 그래프는 데이터의 변동성, 이상치 및 대칭성을 보여주는 데 유용하다.
- **바이올린 플롯(violin plot)**: 바이올린 플롯은 수치 변수의 밀도 분포를 수직축을 따른 대칭 곡선으로 나타내어 보여준다. 바이올린 플롯은 데이터의 다중 양식, 왜도 및 첨도를 보여주는 데 유용하다.
- **군집도(swarm plot)**: 군집도는 수치 변수의 개별 관측치를 범주형 축을 따라 점으로 표시하여 보여준다. 군집도는 데이터의 분포, 밀도 및 이상치를 보여주는 데 유용하다.
- **히트맵(heat ap)**: 히트맵은 변수의 값을 2차원 격자 위에 색으로 나타내어 그 강도를 보여준다. 히트맵은 자료에서 공간적, 시간적 또는 계층적 패턴을 보여주는 데 유용하다.
- **쌍 플롯(pair plot)**: 쌍플롯은 산점도의 행렬로 표시하여 여러 변수 간의 쌍별 관계를 보여준다. 쌍구도는 데이터의 상관관계, 분포 및 이상치를 보여주는 데 유용하다.
- **합동 플롯(joint plot)**: 합동 플롯은 두 변수 사이의 관계를 주변 히스토그램이 있는 산점도로 표시하여 보여준다. 합동 도표는 데이터의 합동 및 주변 분포를 보여주는 데 유용하다.
- **회귀 그래프(regression plot)**: 회귀 그래프는 산점도에 회귀선 또는 곡선을 적합시켜 두 변수 간의 관계를 보여준다. 회귀 그래프는 데이터의 선형 또는 비선형 추세, 기울기 및 잔차를 보여주는 데 유용하다.
- **요인 플롯(factor plot)**: 요인 플롯은 요인이 다른 일련의 부분 플롯으로 표시하여 수치 변수와 범주형 변수 사이의 관계를 보여준다. 요인 플롯은 데이터에서 요인의 상호작용, 대조 및 효과를 보여주는 데 유용하다.

이들은 가장 일반적인 타입 중 일부이지만 만들고 탐색할 수 있는 것들도 훨씬 더 많다. 이 포스팅에서는 Python에서 Matplotlib와 Seaborn 라이브러리를 사용하여 이러한 플롯 중 일부를 만드는 방법을 보일 것이다. 또한 이러한 플롯을 커스텀화하고 결합하여 보다 고급스럽고 정교한 데이터 시각화를 만드는 방법도 다룰 것이다.

## <a name="sec_02"></a> 데이터 시각화를 위해 Python을 사용하는 이유
Python은 데이터 분석과 데이터 과학에 많이 사용되는 프로그래밍 언어이다. Python은 데이터 시각화를 위한 훌륭한 선택이 될 수 있는 많은 장점들을 가지고 있다. 이러한 장점들 중 일부는 다음과 같다.

- **배우고 사용하기 쉽다**: Python 코드를 읽고 쓰기 쉽게 하는 간단하고 표현적인 구문을 가지고 있다. 또한 Python은 크고 활동적인 커뮤니티를 가지고 있어 초보자와 전문가 모두를 위해 많은 리소스와 지원을 제공한다.
- **풍부하고 다양한 라이브러리**: Python은 데이터 조작, 계산, 시각화를 수행하는 데 도움을 줄 수 있는 다양한 라이브러리와 도구를 지원하고 있다. Python에서 데이터 시각화에 가장 널리 사용되는 라이브러리로는 Mathplotlib과 Seaborn이 있으며, 이 포스팅에서 설명할 것이다.
- **유연하며 커스터마이징 가능**: Python을 사용하면 다양한 타입의 플롯과 차트를 다양한 수준의 복잡성과 커스터마이징할 수 있다. 또한 서로 다른 라이브러리와 도구를 결합하고 통합하여 보다 고급스럽고 정교한 데이터 시각화를 만들 수 있다.
- **대화형으로 동적**: Python을 사용하면 사용자의 입력과 이벤트에 대응할 수 있는 대화형으로 동적 데이터 시각화할 수 있다. 또한 Python을 사용하여 웹과 클라우드 기반 데이터 시각화를 하여 온라인에서 액세스하고 공유할 수 있다.

Python이 데이터 시각화를 위한 훌륭한 선택인 이유 중 일부이다. 그러나 Python이 데이터 시각화를 위한 유일한 옵션은 아니다. R, MATLAB, Tableau, Power BI 등 데이터 시각화를 만드는 데 도움을 줄 수 있는 다른 많은 언어와 도구가 있다. 데이터의 타입, 크기, 복잡성, 시각화의 목적과 청중에 따라 데이터 시각화 요구와 선호도에 가장 적절한 도구를 선택할 수 있다.

이 포스팅에서는 Python, 특히 Matplotlib와 Seaborn을 사용하여 데이터 시각화를 수행하는 데 중점을 둘 것이다. 이 라이브러리를 사용하여 기본, 통계 및 고급 플롯과 차트를 만드는 방법을 설명할 것이다. 또한 이러한 플롯을 커스텀화하고 결합하여 보다 복잡하고 상호 작용하는 데이터 시각화를 만드는 방법도 다룰 것이다.

## <a name="sec_02"></a> Matplotlib과 Seaborn 설치와 Import 
Matplotlib와 Seaborn을 사용하여 데이터 시각화를 생성하기 전에 이러한 라이브러리를 Python 환경에 설치하고 임포트하여야 한다. 이 절에서는 이러한 작업을 수행하는 방법에 대해 설명한다.

atplotlib와 Seaborn을 설치하려면 터미널이나 명령 프롬프트에서 pip 명령을 사용하면 된다. pip은 Python 패키지를 설치하고 관리할 수 있는 패키지 관리자이다. atplotlib와 Seaborn을 설치하려면 다음 명령을 실행한다.

```bash
# Install matplotlib
$ pip install matplotlib
# Install seaborn
$ pip install seaborn
```

Matplotlib와 Seaborn을 설치했으면 Python 스크립트나 노트북에 임포트할 수 있다. Matplotlob를 임포트하려면 다음 문장을 사용할 수 있다.

```python
# Import matplotlib
import matplotlib.pyplot as plt
```

`plt` 별칭은 `matplotlib.pyplot` 모듈을 액세스할 수 있는 일반적인 규칙으로, `matplotlib`으로 플롯하는 주요 기능과 방법을 제공한다.

Seaborn을 임포트하려면 다음 문장을 사용할 수 있다.

```python
# Import seaborn
import seaborn as sns
```

`sns` 별칭은 `seaborn` 모듈에 접근할 수 있는 일반적인 관례로, `seaborn`으로 플롯하는 주요 기능과 방법을 제공한다.

이제 Matplotlib와 Seaborn을 설치하고 임포트하여, 이 라이브러리를 사용하여 첫 번째 데이터 시각화를 생성할 준비가 되었다. 다음 절에서는 Matplotlib로 기본 플롯을 생성하는 방법을 설명할 것이다.

## <a name="sec_02"></a> Matplotlib을 사용한 기본 플롯팅
이 절에서는 Matplotlib를 사용하여 선 그래프, 산점도, 막대 그래프, 히스토그램 같은 기본 플롯을 만드는 방법을 설명한다. 제목, 레이블, 범례, 축, 격자 및 주석을 추가하는 것과 같은 Matplotlib을 사용하여 플롯을 커스텀화 방법도 보일 것이다.

Matplotliob을 사용하여 플롯하려면 `plt.plot()` 함수를 사용해야 하며, 이 함수는 플롯할 데이터를 지정하는 하나 이상의 인수를 사용한다. 예를 들어, 숫자 1부터 10까지의 단순 선 그림을 그리는 코드는 다음과 같다.

```python
# Import matplotlib
import matplotlib.pyplot as plt

# Create a simple line plot
plt.plot([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
# Show the plot
plt.show()
```

다음과 같은 플롯이 생성된다.

![](./images/fig_45_01.png)

`plt.plot()` 함수는 x축 값을 0부터 시작하여 자동으로 데이터의 인덱스로 할당한다. x축 값을 지정하려면 `plt.plot()` 함수에 다른 인수를 전달할 수 있다. 예를 들어, 숫자 11부터 20까지의 x축 값과 동일한 데이터를 표시하려면 다음 코드를 사용한다.

```python
# Import matplotlib
import matplotlib.pyplot as plt

# Create a simple line plot with custom x-axis values
plt.plot([11, 12, 13, 14, 15, 16, 17, 18, 19, 20], [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
# Show the plot
plt.show()
```

다음과 같은 플롯이 생성된다.

![](./images/fig_45_02.png)

`plt.plot()` 함수를 여러 번 호출하여 동일한 플롯에 여러 데이터 집합을 표시할 수도 있다. 예를 들어 색상과 스타일이 다른 두 개의 다른 데이터 집합을 표시하려면 다음 코드를 사용할 수 있다.

```python
# Import matplotlib
import matplotlib.pyplot as plt

# Create two data sets
x1 = [1, 2, 3, 4, 5]
y1 = [1, 4, 9, 16, 25]
x2 = [1, 2, 3, 4, 5]
y2 = [1, 8, 27, 64, 125]
# Plot the first data set with a blue solid line
plt.plot(x1, y1, color='blue', linestyle='-')
# Plot the second data set with a red dashed line
plt.plot(x2, y2, color='red', linestyle='--')
# Show the plot
plt.show()
```

다음과 같은 플롯이 생성된다.

![](./images/fig_45_03.png)

보다시피 `plt.plot()` 함수를 사용하면 키워드 인수를 사용하여 플롯의 색상과 라인스타일을 지정할 수 있다. 이러한 인수를 단일 문자열로 결합하기 위해 축약 표기법을 사용할 수도 있다. 예를 들어 위의 코드를 다음과 같이 다시 쓸 수 있다.

```python
# Import matplotlib
import matplotlib.pyplot as plt

# Create two data sets
x1 = [1, 2, 3, 4, 5]
y1 = [1, 4, 9, 16, 25]
x2 = [1, 2, 3, 4, 5]
y2 = [1, 8, 27, 64, 125]
# Plot the first data set with a blue solid line
plt.plot(x1, y1, 'b-')
# Plot the second data set with a red dashed line
plt.plot(x2, y2, 'r--')
# Show the plot
plt.show()
```

이렇게 하면 이전과 동일한 플롯이 생성된다. 축약 표기법은 색상, 마커 및 선 스타일의 세 부분으로 구성되어 있다. 색상은 파란색의 경우 `'b'`, 빨간색의 경우 `'r'`, 녹색의 경우 `'g'` 등 같이 한 글자로 지정할 수 있다. 마커는 점의 경우 `'.'`, 원의 경우 `'o'`, 별의 경우 `'*'` 등 같이 기호로 지정할 수 있습다. 선 스타일은 실선의 경우 `'-'`, 점선의 경우 `'--'`, 대시 점의 경우 `'-.'` 등과 같이 대시와 점의 시퀀스로 지정할 수 있다. matplotlib 설명서에서 이러한 인수에 대해 가능한 값의 전체 [목록](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.plot.html)을 찾을 수 있다.

matplotlib로 플롯을 만드는 기본적인 방법들이다. 다음 절에서는 제목, 레이블, 범례, 축, 격자, 주석을 추가하는 등 matplotlib로 플롯을 커스터마이징하는 방법을 설명할 것이다.

## <a name="sec_02"></a> Matplotlib에서 플롯 커스터마이징
이 절에서는 제목, 레이블, 범례, 축, 격자 및 주석을 추가하는 등 Matplotlib로 플롯을 커스터마이징하는 방법을 설명한다. Matplotlib로 플롯의 크기, 스타일 및 형식을 변경하는 방법도 다룰 것이다.

Matplotlib을 사용하여 플롯을 커스터마이즈하려면 플롯의 모양과 동작을 수정할 수 있는 `plt` 함수와 메서드를 사용해야 한다. 예를 들어 제목, x축 레이블과 y축 레이블을 그림에 추가하려면 다음 함수를 사용할 수 있다.

```python
# Import matplotlib
import matplotlib.pyplot as plt

# Create a simple line plot
plt.plot([1, 2, 3, 4, 5], [1, 4, 9, 16, 25])
# Add a title
plt.title("A Simple Line Plot")
# Add an x-axis label
plt.xlabel("X")
# Add a y-axis label
plt.ylabel("Y")
# Show the plot
plt.show()
```

다음과 같은 플롯이 생성된다.

![](./images/fig_45_04.png)

보다시피 `plt.title()`, `plt.xlabel()` 및 `plt.ylabel()` 함수를 사용하면 제목과 레이블을 플롯에 추가할 수 있다. 또한 이러한 함수에 추가 인수를 전달하여 텍스트의 글꼴 크기, 색상, 정렬 및 스타일을 변경할 수도 있다. 가능한 인수의 전체 [목록](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.plot.html)은 matplotlib 설명서에서 찾을 수 있다.

플롯에 범례를 추가하려면 `plt.legend()` 함수를 사용해야 하며, 이 함수는 표시한 데이터 집합에 해당하는 레이블 목록을 가져온다. 예를 들어 색상과 스타일이 다른 두 데이터 집합의 플롯에 범례를 추가하려면 다음과 같이 할 수 있다.

```python
# Import matplotlib
import matplotlib.pyplot as plt

# Create two data sets
x1 = [1, 2, 3, 4, 5]
y1 = [1, 4, 9, 16, 25]
x2 = [1, 2, 3, 4, 5]
y2 = [1, 8, 27, 64, 125]
# Plot the first data set with a blue solid line
plt.plot(x1, y1, 'b-')
# Plot the second data set with a red dashed line
plt.plot(x2, y2, 'r--')
# Add a legend
plt.legend(["First Data Set", "Second Data Set"])
# Show the plot
plt.show()
```

다음과 같은 플롯이 생성된다.

![](./images/fig_45_05.png)

보다시피 `plt.legend()` 함수를 사용하면 플롯에 범례를 추가할 수 있다. 이 함수에 추가 인수를 전달하여 범례의 위치, 크기 및 스타일을 변경할 수도 있다. 가능한 인수의 전체 [목록](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.plot.html)은 Matplotlib 설명서에서 찾을 수 있다.

축, 격자 및 주석을 그림에 추가하려면 각각 `plt.axis()`, `plt.grid()` 및 `plt.annotate()` 함수를 사용해야 한다. 예를 들어 색상과 스타일이 다른 두 데이터 세트의 플롯에 축, 격자 및 주석을 추가하려면 다음과 같이 코드를 작성할 수 있다.

```python
# Import matplotlib
import matplotlib.pyplot as plt

# Create two data sets
x1 = [1, 2, 3, 4, 5]
y1 = [1, 4, 9, 16, 25]
x2 = [1, 2, 3, 4, 5]
y2 = [1, 8, 27, 64, 125]
# Plot the first data set with a blue solid line
plt.plot(x1, y1, 'b-')
# Plot the second data set with a red dashed line
plt.plot(x2, y2, 'r--')
# Add axes
plt.axis([0, 6, 0, 150])
# Add grids
plt.grid(True)
# Add annotations
plt.annotate("A Point of Interest", xy=(3, 27), xytext=(4, 50), arrowprops=dict(facecolor='black', shrink=0.05))
# Show the plot
plt.show()
```

다음과 같은 플롯이 생성된다.

![](./images/fig_45_06.png)

보시다시피 `plt.axis()` 함수를 사용하면 `[xmin, xmax, ymin, ymax]` 네 값의 리스트를 전달하여 x축과 y축의 한계를 설정할 수 있다. `plt.grid()` 함수를 사용하면 `True` 또는 `False` 부울 값을 전달하여 플롯에 그리드를 추가할 수 있다. `plt.annotate()` 함수를 사용하면 텍스트, 관심 지점 및 옵션 화살표를 전달하여 플롯에 주석을 추가할 수 있다. 축, 그리드 및 주석의 모양과 동작을 변경하기 위해 이러한 함수에 추가 인수를 전달할 수도 있다. Matplotlib 설명서에서 가능한 인수의 전체 [목록](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.plot.html)을 찾을 수 있다.

Matplotlib을 사용하여 플롯을 커스터마이징하는 방법은 다음과 같다. 다음 절에서는 Matplotlib을 사용하여 플롯의 크기, 스타일 및 형식을 변경하는 방법을 다룰 것이다.

## <a name="sec_02"></a> Seaborn을 사용한 통계 플롯
이 절에서는 상자 그래프, 바이올린 그래프, 군집 플롯(swarm plot), 히트맵 같은 Seaborn으로 통계 그림을 만드는 방법을 설명한다. 플롯의 색상, 스타일 및 크기를 변경하는 것과 같이 Seaborn으로 플롯을 커스터마이징 방법도 다룬다.

Seaborn으로 플롯을 하려면 다양한 타입의 데이터와 통계량을 표시할 수 있는 `sns` 함수와 메서드를 사용해야 한다. 예를 들어, 숫자 변수의 분포에 대한 상자 그래프를 작성하려면 `sns.boxplot()` 함수를 사용하여 표시할 데이터를 지정하는 하나 이상의 인수를 사용한다. 예를 들어, 식당에서 손님이 주는 팁에 대한 정보를 포함하는 Seaborn에 내장된 데이터 집합인 팁 데이터 집합의 상자 그래프를 다음과 같이 작성할 수 있다.

```python
# Import matplotlib and seaborn
import matplotlib.pyplot as plt
import seaborn as sns

# Load the tips dataset
tips = sns.load_dataset("tips")
# Create a box plot of the total bill
sns.boxplot(x=tips["total_bill"])
# Show the plot
plt.show()
```

다음과 같은 그래프가 생성된다.

![](./images/fig_45_07.png)

보다시피 `sns.boxplot()` 함수를 사용하면 수치 변수의 분포에 대한 상자 그래프를 만들 수 있다. 이 함수에 추가 인수를 전달하여 그래프의 방향, 색상 및 스타일을 변경할 수 있다. 가능한 인수의 전체 [목록](https://seaborn.pydata.org/api.html)은 Seaborn 문서에서 찾을 수 있다.

수치 변수의 분포에 대한 바이올린 플롯을 만들고 싶다면 `sns.violinplot()` 함수를 사용할 수 있으며, 이 함수는 `sns.boxplot()` 함수와 동일한 인수를 사용한다. 예를 들어 전체 지폐의 바이올린 플롯을 만들고자 하면 다음과 같이 작성할 수 있다.

```python
# Import matplotlib and seaborn
import matplotlib.pyplot as plt
import seaborn as sns

# Load the tips dataset
tips = sns.load_dataset("tips")
# Create a violin plot of the total bill
sns.violinplot(x=tips["total_bill"])
# Show the plot
plt.show()
```

다음과 같은 플롯이 생성된다.

![](./images/fig_45_08.png)

보다시피 `sns.violinplot()` 함수를 사용하면 수치 변수의 분포에 대한 바이올린 플롯을 만들 수 있다. 이 함수에 추가 인수를 전달하여 플롯의 방향, 색상 및 스타일을 변경할 수도 있다. 가능한 인수의 전체 [목록](https://seaborn.pydata.org/api.html)은 Seaborn 문서에서 찾을 수 있다.

수치 변수의 개별 관측치에 대한 군집 플롯을 작성하려면 `sns.boxplot()`과 `sns.violinplot()` 함수와 동일한 인수를 사용하는 `sns.swarmplot()` 함수를 사용할 수 있다. 예를 들어 전체 지폐의 군집 플롯을 작성하려면 다음과 같은 코드를 작성할 수 있다.

```python
# Import matplotlib and seaborn
import matplotlib.pyplot as plt
import seaborn as sns

# Load the tips dataset
tips = sns.load_dataset("tips")
# Create a swarm plot of the total bill
sns.swarmplot(x=tips["total_bill"])
# Show the plot
plt.show()
```

다음과 같은 플롯이 생성된다.

![](./images/fig_45_09.png)

보다시피 `sns.swarmplot()` 함수를 사용하면 수치 변수의 개별 관측치의 군집 플롯을 만들 수 있다. 이 함수에 추가 인수를 전달하여 플롯의 방향, 색상 및 스타일을 변경할 수도 있다. 가능한 인수의 전체 [목록](https://seaborn.pydata.org/api.html)은 Seaborn 문서에서 찾을 수 있다.

이들은 Seaborn으로 통계적 그래프를 만드는 기본적인 방법들 중 일부이다. 다음 절에서, Seaborn으로 쌍 플롯, 합동 플롯, 회귀 그래프, 그리고 요인 플롯과 같은 고급 플롯을 만드는 방법을 설명할 것이다.

## <a name="sec_02"></a> Seaborn을 사용한 고급 플롯
이 절에서는 쌍 플롯, 합동 플롯, 회귀 그래프, 그리고 요인 플롯 등 Seaborn으로 고급 플롯을 작성하는 방법을 설명할 것이다. 또한 플롯의 색상, 스타일 및 크기를 변경하는 등 Seaborn으로 플롯을 커스터마이징하는 방법도 보인다.

Seaborn으로 플롯을 만들려면 다양한 타입의 데이터와 통계량을 표시할 수 있는 `sns` 함수와 방법을 사용해야 한다. 예를 들어, 여러 변수 간의 쌍 관계에 대한 쌍 플롯을 작성하려면 `sns.pairplot()` 함수를 사용하여 표시할 데이터를 지정하는 하나 이상의 인수를 사용한다. 예를 들어, 레스토랑에서 고객이 제공하는 팁에 대한 정보를 포함하는 Seaborn에 내장된 데이터 집합인 팁 데이터 집합의 쌍 플롯을 작성하려면 다음 코드를 사용한다.

```python
# Import matplotlib and seaborn
import matplotlib.pyplot as plt
import seaborn as sns

# Load the tips dataset
tips = sns.load_dataset("tips")
# Create a pair plot of the tips dataset
sns.pairplot(tips)
# Show the plot
plt.show()
```

다음과 같은 플롯이 생성된다.

![](./images/fig_45_10.png)

보다시피 `sns.pairplot()` 함수를 사용하면 여러 변수 간의 쌍대 관계에 대한 쌍대 그림을 만들 수 있다. 이 함수에 추가 인수를 전달하여 플롯의 색상, 스타일 및 크기를 변경할 수도 있다. 가능한 인수의 전체 [목록](https://seaborn.pydata.org/api.html)은 Seaborn 문서에서 찾을 수 있다.

두 변수 간의 관계에 대한 합동 플롯을 작성하려면 `sns.jointplot()` 함수를 사용할 수 있으며, 이 함수는 `sns.pairplot()` 함수와 동일한 인수를 사용한다. 예를 들어 전체 지폐와 팁의 합동 그림을 작성하려면 다음 코드를 사용할 수 있다.

```python
# Import matplotlib and seaborn
import matplotlib.pyplot as plt
import seaborn as sns

# Load the tips dataset
tips = sns.load_dataset("tips")
# Create a joint plot of the total bill and the tip
sns.jointplot(x="total_bill", y="tip", data=tips)
# Show the plot
plt.show()
```

다음과 같은 플롯이 생성된다.

![](./images/fig_45_11.png)

보다시피 `sns.jointplot()` 함수를 사용하면 두 변수 간의 관계에 대한 합동 플롯을 만들 수 있다. 이 함수에 추가 인수를 전달하여 플롯의 색상, 스타일 및 크기를 변경할 수 있다. 가능한 인수의 전체 [목록](https://seaborn.pydata.org/api.html)은 Seaborn 문서에서 찾을 수 있다.

두 변수 간의 관계에 대한 회귀 그래프를 작성하려면 `sns.regplot()` 함수를 사용하고 `sns.jointplot()` 함수와 동일한 인수를 사용한다. 예를 들어 전체 청구서와 팁의 회귀 그래프 작성하려면 다음 코드를 사용할 수 있다.

```python
# Import matplotlib and seaborn
import matplotlib.pyplot as plt
import seaborn as sns

# Load the tips dataset
tips = sns.load_dataset("tips")
# Create a regression plot of the total bill and the tip
sns.regplot(x="total_bill", y="tip", data=tips)
# Show the plot
plt.show()
```

다음과 같은 그래프가 생성된다.

![](./images/fig_45_12.png)

보다시피 `sns.regplot()` 함수를 사용하면 두 변수 간의 관계에 대한 회귀 그레프를 만들 수 있다. 이 함수에 추가 인수를 전달하여 그패프의 색상, 스타일 및 크기를 변경할 수도 있다. 가능한 인수의 전체 [목록](https://seaborn.pydata.org/api.html)은 Seaborn 문서에서 찾을 수 있다.

이들은 Seaborn으로 고급 플롯을 만드는 몇 가지 방법이다. 다음 절에서는 Matplotlib와 Seaborn을 결합하여 서브플롯(subplot), 패싯 그리드(facet grid), 관계 플롯(rel plot) 같이 더 복잡하고 상호작용적인 플롯을 만드는 방법을 설명할 것이다.

## <a name="sec_02"></a> Matplotlib과 Seaborn 결합
이 절에서는 서브플롯(subplot), 패싯 그리드(facet grid), 관계 플롯(rel plot) 같이 더 복잡하고 상호작용적인 플롯을 만드는 방법을 설명한다. 또한 플롯의 색상, 스타일 및 크기를 변경하는 것과 같이 Matplotlib와 Seaborn을 사용하여 그림을 커스텀화하는 방법도 보인다.

Matplotlib와 Seaborn을 결합하려면 다양한 유형의 플롯과 차트를 만들고 수정할 수 있는 `plt`와 `sns` 함수와 메서드를 사용해야 한다. 예를 들어, 4개의 다른 플롯으로 서브플롯을 작성하려면 서브플롯의 행 수, 열 및 인덱스를 지정하는 3개의 인수를 사용하는 `plt.subplot()` 함수를 사용할 수 있다. 예를 들어, 팁 데이터 집합의 4개의 다른 그림을 작성하려면 다음 코드를 사용할 수 있다.

```python
# Import matplotlib and seaborn
import matplotlib.pyplot as plt
import seaborn as sns

# Load the tips dataset
tips = sns.load_dataset("tips")
# Create a subplot of four plots
plt.subplot(2, 2, 1) # The first subplot in a 2x2 grid
sns.boxplot(x=tips["total_bill"]) # A box plot of the total bill
plt.subplot(2, 2, 2) # The second subplot in a 2x2 grid
sns.violinplot(x=tips["total_bill"]) # A violin plot of the total bill
plt.subplot(2, 2, 3) # The third subplot in a 2x2 grid
sns.swarmplot(x=tips["total_bill"]) # A swarm plot of the total bill
plt.subplot(2, 2, 4) # The fourth subplot in a 2x2 grid
sns.distplot(tips["total_bill"]) # A histogram of the total bill
# Show the plot
plt.show()
```

다음과 같은 플롯이 생성된다.

![](./images/fig_45_13.png)

보다시피 `plt.subplot()` 함수를 사용하면 2x2 격자 내에 4개의 그림으로 이루어진 서브플롯을 만들 수 있다. 이 함수에 추가 인수를 전달하여 서브플롯의 크기와 간격을 변경할 수 있다. 가능한 인수의 전체 목록은 Matplotlib 문서에서 찾을 수 있다.

요인이 다른 여러 플롯의 패싯 그리드를 작성하려면 `sns.FacetGrid()` 함수를 사용하면 된다. 예를 들어 고객의 성별에 대해 색상이 다른 팁과 요일의 패싯 그리드를 작성하려면 다음 코드를 사용할 수 있다.

```python
# Import matplotlib and seaborn
import matplotlib.pyplot as plt
import seaborn as sns

# Load the tips dataset
tips = sns.load_dataset("tips")
# Create a facet grid of the tip and the day of the week, with different colors for the sex of the customer
g = sns.FacetGrid(tips, col="day", hue="sex") # A facet grid with four columns for each day and different colors for each sex
g.map(sns.swarmplot, "tip") # A swarm plot of the tip for each facet
g.add_legend() # A legend for the colors
# Show the plot
plt.show()
```

다음과 같은 플롯이 생성된다.

![](./images/fig_45_14.png)

보다시피 `sns.FacetGrid()` 함수를 사용하면 요인이 다른 여러 개의 플롯의 패싯 그리드를 만들 수 있다. 이 함수에 추가 인수를 전달하여 플롯의 색상, 스타일 및 크기를 변경할 수 있다. 가능한 인수의 전체 목록은 Seaborn 문서에서 찾을 수 있다.

다른 측면과 수준을 사용하여 두 변수 간의 관계에 대한 관계도를 작성하려면 `sns.relplot()` 함수를 사용한다. 예를 들어 고객의 성별에 따라 다른 색상을 사용하고 하루 중 다른 시간에 대해 다른 열을 사용하여 전체 청구서와 팁의 관계도를 작성하려면 다음 코드를 사용한다.

```python
# Import matplotlib and seaborn
import matplotlib.pyplot as plt
import seaborn as sns

# Load the tips dataset
tips = sns.load_dataset("tips")
# Create a rel plot of the total bill and the tip, with different colors for the sex of the customer and different columns for the time of the day
sns.relplot(x="total_bill", y="tip", data=tips, hue="sex", col="time") # A rel plot with two columns for each time and different colors for each sex
# Show the plot
plt.show()
```

다음과 같은 플롯이 생성된다.

![](./images/fig_45_15.png)

보다시피 `sns.relplot()` 함수를 사용하면 양상과 수준이 다른 두 변수 사이의 관계에 대한 관계 플롯을 만들 수 있다. 이 함수에 추가 인수를 전달하여 플롯의 색상, 스타일 및 크기를 변경할 수 있다. 가능한 인수의 전체 목록은 Seaborn 문서에서 찾을 수 있다.

Matplotlib와 Seaborn을 결합하여 좀 더 복잡하고 상호작용적인 플롯을 만드는 몇 가지 방법이다. 다음 절에서는 튜토리얼을 마무리한다.

## <a name="summary"></a> 요약
Matplotlib와 Seaborn을 이용한 Python 데이터 시각화에 대한 이 포스팅에서는 다음과 같은 방법을 설명했다.

- Matplotlib와 Seaborn 설치와 임포트
- 선 그래프, 산점도, 막대 그래프 및 히스토그램 같은 Matplotlib을 사용하여 기본 플롯 생성
- 제목, 레이블, 범례, 축, 격자 및 주석 추가와 같은 Matplotlib을 사용하여 플롯 커스터마이징
- Matplotlib을 사용하여 플롯의 크기, 스타일 및 형식 변경
- 상자 그래프, 바이올린 플롯, 군집도, 히트맵과 같은 Seaborn으로 통계도 작성
- 쌍 플롯, 합동 플롯, 회귀 그래프 및 요인 플롯과 같은 Seaborn으로 고급 플롯 작성
- 플롯의 색상, 스타일 및 크기를 변경하는 등 플롯 커스터마이징
- Matplotlib와 Seaborn을 결합하여 서브플롯, 패싯 그리드 및 관계 플롯과 같이 더 복잡하고 상호작용적인 플롯 생성

이 포스팅을 학습함으로써 Python에서 Matplotlib와 Seaborn 라이브러리를 사용하여 데이터 시각화를 하는 데 도움이 될 수 있는 기술과 지식을 얻었다. 또한 여러분의 데이터를 명확하고 효과적인 방법으로 이해하고 탐색하고 전달하는 데 도움이 될 수 있는 다양한 유형의 플롯과 차트를 만드는 데 이러한 라이브러리를 사용하는 방법의 몇 가지 예도 보았다.
